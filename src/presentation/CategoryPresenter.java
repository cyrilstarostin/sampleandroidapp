package presentation;

import domain.FilterParams;
import domain.Product;
import domain.SortType;

public class CategoryPresenter {

    private CategoryView view;

    void firstAttachView(CategoryView view) {
        this.view = view;
        loadAndShowProducts();
    }

    private void loadAndShowProducts() {
        // TODO: load and show products from the net
    }

    void onFilterApplied(FilterParams params) {
        // TODO: get filtered products and show them
    }

    void onSortChosen(SortType sortType) {
        // TODO: get sorted products and show them
    }

    void onProductAddToCartClick(Product product) {
        // TODO: make server request to add product
        // if it's ok, then increase product count and update it
    }

    void onProductRemoveFromCartClick(Product product) {
        // TODO: make server request to remove product
        // if it's ok, then increase product count and update it
    }

    void onSearchProducts(String query) {
        // TODO: search products and show what is found
    }
}
