package presentation;

import domain.Product;

import java.util.List;

public interface CategoryView {

    void showLoading();

    void showNoProductsFound();

    void showProducts(List<Product> products);

    void updateProduct(Product product);
}
