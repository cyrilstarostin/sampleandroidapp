package domain;

public enum SortType {
    CHEAPEST, MOST_EXPENSIVE, BY_NAME
}
