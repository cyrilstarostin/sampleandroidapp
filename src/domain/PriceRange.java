package domain;

public class PriceRange {
    public float lowest;
    public float highest;

    public PriceRange(float lowest, float highest) {
        this.lowest = lowest;
        this.highest = highest;
    }
}
