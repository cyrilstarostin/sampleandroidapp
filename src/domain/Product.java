package domain;

public class Product {
    public int id;
    public String name;
    public float price;
    public ShopType shopType;
}
